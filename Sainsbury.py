from lxml import html
#from scrapy import Selector
#from scrapy.http import HtmlResponse
import requests


class Sainsbury:

 page          = requests.get('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html')
 tree          = html.fromstring(page.content) 
 anchors       = tree.xpath('//div[@class="productInfo"]//a/@href')
 products      = tree.xpath('//div[@class="productInfo"]/h3/a/text()')
 priceperUnit  = tree.xpath('//div[@class="pricing"]//p[@class="pricePerUnit"]/text()') 
 resdics       = {} 

 def __init__(self): # this method creates the class object.
    pass
 #Auxiliary function to Print out in JSON format             
 def printJSON(self,elem,indlist,listsize):

    res = ""
    for index in elem.split(','):  
      if indlist != (listsize-1):    
        res = res+index +","+"\n"
      if indlist == (listsize-1):
        res = res+index +""+"\n"   
    return res     
    
 def parser(self,anchorlist,resdics):   
    resJson = ""
    
    if len(anchorlist) > 0:
      total = 0
      for link in anchorlist:   
        pagelink =  requests.get(link)
        pagecontent = html.fromstring(pagelink.content)
         
        self.resdics['title']       = pagecontent.xpath('//div[@class="productTitleDescriptionContainer"]/h1/text()')[0] 
        self.resdics['size']        = str(float(pagelink.headers['Content-Length'])/1000) + "kb"
        self.resdics['description'] = pagecontent.xpath('//div[@class="productText"]/p/text()')[0] 
        self.resdics['unit_price']  = float((pagecontent.xpath('//div[@class="pricing"]//p[@class="pricePerUnit"]/text()')[0]).replace(u"\xA3", ''))
        total = total + self.resdics['unit_price']
        res = self.printJSON(str(self.resdics),anchorlist.index(link),len(anchorlist))
        resJson = resJson+ " " +res+"\n"
  
    print "{"+"\n"+"'results'"+":["+"\n\n"+resJson+"\n"+"],"

    print "'total':"+str(total) 
    print "\n"+"}"
                 
if __name__ == "__main__":
    
    s = Sainsbury()
    s.parser(s.anchors,s.resdics)
     
               
            
            