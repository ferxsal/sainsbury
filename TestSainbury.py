import unittest
from lxml import html
import requests
from selenium import webdriver


class MyTest(unittest.TestCase):
    
    page = requests.get('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html')
    tree = html.fromstring(page.content) 
    anchors       = tree.xpath('//div[@class="productInfo"]//a/@href')
    
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        #driver = webdriver.Chrome()
    
    def test_title(self):
        self.driver.get('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html')
        self.assertEqual(self.driver.title,"Ripe & ready | Sainsbury's")
    
        
    def test_links(self):
        
        title       = '//div[@class="productTitleDescriptionContainer"]/h1/text()'
        description = '//div[@class="productText"]/p/text()'
        
        firstweblink = requests.get(self.anchors[0])
        pagecontent =  html.fromstring(firstweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Apricot Ripe & Ready x5") 
        self.assertEqual(pagecontent.xpath(description)[0],"Apricots")
         
        secondweblink = requests.get(self.anchors[1])
        pagecontent =  html.fromstring(secondweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Avocado Ripe & Ready XL Loose 300g")
        self.assertEqual(pagecontent.xpath(description)[0],"Avocados")
        
         
        thirdweblink = requests.get(self.anchors[2])
        pagecontent =  html.fromstring(thirdweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Avocado, Ripe & Ready x2")
        self.assertEqual(pagecontent.xpath(description)[0],"Avocados")
         
         
        fourthweblink = requests.get(self.anchors[3])
        pagecontent =  html.fromstring(fourthweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Avocados, Ripe & Ready x4")
        self.assertEqual(pagecontent.xpath(description)[0],"Avocados")
        
                          
        fithweblink = requests.get(self.anchors[4])
        pagecontent =  html.fromstring(fithweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Conference Pears, Ripe & Ready x4 (minimum)")
        self.assertEqual(pagecontent.xpath(description)[0],"Conference")
                          
        sixweblink = requests.get(self.anchors[5])
        pagecontent =  html.fromstring(sixweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Golden Kiwi x4")
        self.assertEqual(pagecontent.xpath(description)[0],"Gold Kiwi")
        
         
        sevenweblink = requests.get(self.anchors[6])
        pagecontent =  html.fromstring(sevenweblink.content)
        self.assertEqual(pagecontent.xpath(title)[0],"Sainsbury's Kiwi Fruit, Ripe & Ready x4")
        self.assertEqual(pagecontent.xpath(description)[0],"Kiwi") 
           
    
    def testNumberProducts(self):
        self.assertEqual(len(self.anchors), 7)
     
        
    def testPricing(self):
        total = 0
        for link in self.anchors:   
         pagelink =  requests.get(link)
         pagecontent = html.fromstring(pagelink.content)
         prices = float((pagecontent.xpath('//div[@class="pricing"]//p[@class="pricePerUnit"]/text()')[0]).replace(u"\xA3", ''))
         total = total + prices
        
        self.assert_(total, 15.1)
            
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()    
          